/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package emush;

/**
 *
 * @author User
 */
public class Joueur {

    /**
     * @param args the command line arguments
     */
            
    private String nom;
    private int pointsDeVie;
    private int pointsDAction;
    private int pointsDeMouvement;
    private int pointsMorals;

    public Joueur(String nom) {
        this.nom = nom;
        //peut etre changer
        this.pointsDeVie = 14;
        this.pointsDAction = 12; 
        this.pointsDeMouvement = 12; 
        this.pointsMorals = 14; 
    }

    // Méthode pour subir des dégâts
    public void recevoirDegats(int degats) {
        this.pointsDeVie -= degats;
        if (this.pointsDeVie < 0) {
            this.pointsDeVie = 0;
            // Logique pour gérer la "mort" du joueur
        }
    }

    // Méthode pour se soigner
    public void soigner(int soin) {
        this.pointsDeVie += soin;//healingPoints
        // Assurez-vous que les points de vie ne dépassent pas le maximum
    }
    
    public void useActionPoints(int points) {
        this.pointsDAction -= points;
        // Vous pouvez ajouter une logique pour gérer le cas où il ne reste pas assez de PA
    }

    // Getters et Setters
    public String getNom() { 
        return nom;
    }
    public int getPointsDeVie() { 
        return pointsDeVie;
    }
    // Continuez avec les autres getters et setters...

    // Autres méthodes selon les besoins du jeu

    }
    

