/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emush;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class Salle {
    private String nom;
    private List<Objet> objets;

    public Salle(String nom) {
        this.nom = nom;
        this.objets = new ArrayList<>();
    }

    // Méthode pour ajouter un objet a la salle
    public void ajouterObjet(Objet objet) {
        this.objets.add(objet);
    }

    // Méthode pour retirer un objet
    /*public Objet retirerObjet(String nomObjet) {
     
    }*/

    // Getters et Setters
    public String getNom() { 
        return nom;
    }
    // Continuez avec les autres getters et setters...

    // Autres méthodes selon les besoins du jeu
}

    
