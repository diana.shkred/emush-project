/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emush;

/**
 *
 * @author User
 */
public class Objet {
    
    private String nom;
    private String description;

    public Objet(String nom, String description) {
        this.nom = nom;
        this.description = description;
    }

    // Méthode pour utiliser l'objet
    public void utiliser(Joueur joueur) {
        // Logique d'utilisation de l'objet
    }

    // Getters et Setters
    public String getNom() { 
        return nom; 
    }
    public String getDescription() { 
        return description; 
    }
    // Continuez avec les autres getters et setters...

    // Autres méthodes selon les besoins du jeu
}

