/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emush;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class Vaisseau {
    
    
    private List<Salle> salles;
    private int niveauDeBouclier;
    private int niveauDOxygene;
    private int niveauDeCarburant;

    public Vaisseau() {
        this.salles = new ArrayList<>();
        // Initialiser le vaisseau avec des valeurs par défaut
        this.niveauDeBouclier = 200; // Initialisation avec une valeur arbitraire
        this.niveauDOxygene = 500; // Initialisation avec une valeur arbitraire
        this.niveauDeCarburant = 50; // Initialisation avec une valeur arbitraire
    }

    // Gestion du bouclier
    public void endommagerBouclier(int degats) {
        niveauDeBouclier -= degats;
        if (niveauDeBouclier < 0) {
            niveauDeBouclier = 0; // Assurer que le niveau ne tombe pas en dessous de 0
        }
    }

    public void reparerBouclier(int reparation) {
        niveauDeBouclier += reparation;
        // Vous pouvez définir une limite maximale pour le bouclier si nécessaire
    }

    // Gestion de l'oxygène
    public void consommerOxygene(int quantite) {
        niveauDOxygene -= quantite;
        if (niveauDOxygene < 0) {
            niveauDOxygene = 0; // Assurer que le niveau ne tombe pas en dessous de 0
        }
    }

    public void ajouterOxygene(int quantite) {
        niveauDOxygene += quantite;
        // définir une limite maximale pour l'oxygène si nécessaire
    }

    // Gestion du carburant
    public void consommerCarburant(int quantite) {
        niveauDeCarburant -= quantite;
        if (niveauDeCarburant < 0) {
            niveauDeCarburant = 0; // Assurer que le niveau ne tombe pas en dessous de 0
        }
    }

    public void ajouterCarburant(int quantite) {
        niveauDeCarburant += quantite;
        // Vous pouvez définir une limite maximale pour le carburant si nécessaire
    }

    // Méthodes pour ajouter ou retirer des salles au vaisseau
    public void ajouterSalle(Salle salle) {
        this.salles.add(salle);
    }

    // Getters et setters pour accéder et modifier les attributs

    
}
