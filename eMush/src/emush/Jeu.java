/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package emush;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class Jeu {

    /**
     * @param args the command line arguments
     */
    private Vaisseau vaisseau;
    private List<Joueur> joueurs;
    private int cycleActuel;

    public Jeu() {
        this.vaisseau = new Vaisseau();
        this.joueurs = new ArrayList<>();
        this.cycleActuel = 0; // Commencer le jeu au cycle 0
    }

    // Méthode pour démarrer le jeu
    public void demarrerJeu() {
        // Initialisation du jeu (Ajouter des joueurs, placer des objets, etc.)
        System.out.println("Le jeu commence !");
    }

    // Méthode pour ajouter un joueur au jeu
    public void ajouterJoueur(Joueur joueur) {
        joueurs.add(joueur);
    }

    // Méthode pour faire avancer le jeu d'un cycle
    public void prochainCycle() {
        cycleActuel++;
        System.out.println("Nous sommes maintenant au cycle " + cycleActuel);

        // Ici, vous pouvez insérer la logique pour gérer ce qui se passe à chaque cycle.
        // Par exemple, diminuer les ressources du vaisseau, permettre aux joueurs de jouer leur tour, etc.

        for (Joueur joueur : joueurs) {
            // Simuler une action pour chaque joueur.
            // Exemple : joueur.effectuerAction();
        }
         System.out.println("Un nouveau cycle commence.");
        // Vous pouvez également gérer des événements aléatoires qui surviennent à chaque cycle.
    }

    // Méthodes supplémentaires selon les besoins du jeu...

}
