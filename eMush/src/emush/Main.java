/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package emush;

/**
 *
 * @author User
 */
public class Main {

    /**
     * @param args the command line arguments
     */

    public static void main(String[] args) {
        // Initialisation du vaisseau
        Vaisseau vaisseau = new Vaisseau();

        /*Création et ajout de salles dans le vaisseau
        exemple
        Salle pont = new Salle("")

        Création et ajout d'objets dans les salles*/
       

        // Création des joueurs
        Joueur wang = new Joueur("Wang Chao");
      

        // Initialisation du jeu
        Jeu jeu = new Jeu();
        jeu.ajouterJoueur(wang);
       

        // Exemple de cycle de jeu
        System.out.println("Début du cycle 1");
        jeu.prochainCycle();

    }
}
    

